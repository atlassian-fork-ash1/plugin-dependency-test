package com.atlassian.jira.tests.dependency.plugin.twothousand.impl;

import com.atlassian.jira.tests.dependency.plugin.twothousand.api.TwoThousandPluginComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import javax.inject.Named;

@ExportAsService ({TwoThousandPluginComponent.class})
@Named ("myPluginComponent")
public class TwoThousandPluginComponentImpl implements TwoThousandPluginComponent {
}
